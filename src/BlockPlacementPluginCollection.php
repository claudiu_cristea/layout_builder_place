<?php

namespace Drupal\layout_builder_place;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Plugin\DefaultSingleLazyPluginCollection;

/**
 * Provides a collection of block placements plugins.
 */
class BlockPlacementPluginCollection extends DefaultSingleLazyPluginCollection {

  /**
   * The block placement ID this plugin collection belongs to.
   *
   * @var string
   */
  protected $blockPlacementId;

  /**
   * Constructs a new plugin collection instance.
   *
   * @param \Drupal\Component\Plugin\PluginManagerInterface $manager
   *   The manager to be used for instantiating plugins.
   * @param string $instance_id
   *   The ID of the plugin instance.
   * @param array $configuration
   *   An array of configuration.
   * @param string $block_placement_id
   *   The unique ID of the block placemenet entity using this plugin.
   */
  public function __construct(PluginManagerInterface $manager, ?string $instance_id, array $configuration, ?string $block_placement_id) {
    parent::__construct($manager, $instance_id, $configuration);
    $this->blockPlacementId = $block_placement_id;
  }

  /**
   * {@inheritdoc}
   */
  public function &get($instance_id): BlockPluginInterface {
    return parent::get($instance_id);
  }

  /**
   * {@inheritdoc}
   */
  protected function initializePlugin($instance_id): void {
    if (!$instance_id) {
      throw new PluginException("The block placement '{$this->blockPlacementId}' did not specify a plugin.");
    }

    try {
      parent::initializePlugin($instance_id);
    }
    catch (PluginException $e) {
      $module = $this->configuration['provider'];
      // Ignore block placements belonging to uninstalled modules, but re-throw
      // valid exceptions when the module is installed and the plugin is
      // misconfigured.
      if (!$module || \Drupal::moduleHandler()->moduleExists($module)) {
        throw $e;
      }
    }
  }

}
