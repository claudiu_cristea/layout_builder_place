<?php

namespace Drupal\layout_builder_place\EventSubscriber;

use Drupal\Component\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\layout_builder\Event\SectionBuildRegionsRenderArrayEvent;
use Drupal\layout_builder\Plugin\DataType\SectionData;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Listens to section regions render array building.
 */
class LayoutBuilderPlaceSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      SectionBuildRegionsRenderArrayEvent::class => 'provideComponents',
    ];
  }

  /**
   * Injects components provided by third-party into section.
   *
   * @param \Drupal\layout_builder\Event\SectionBuildRegionsRenderArrayEvent $event
   *   The event object.
   */
  public function provideComponents(SectionBuildRegionsRenderArrayEvent $event): void {
    $contexts = $event->getContexts();
    $section = $event->getSection();
    $layout = $section->getLayout($contexts);
    $bs = \Drupal::entityTypeManager()->getStorage('block_placement');
    $cp = \Drupal::getContainer()->get('layout_builder_place.context');

    //$cp->setSectionDelta($event->getContexts()['section_delta']->getContextValue());
    foreach ($layout->getPluginDefinition()->getRegionNames() as $region) {
      $cp->setRegion($region);
      /** @var \Drupal\layout_builder_place\Entity\BlockPlacementInterface $block_placement */
      foreach ($bs->loadByProperties(['status' => TRUE]) as $block_placement) {
        $access = $block_placement->access('view');
      }
    }

//    $contexts = [
//      'section' => new ContextDefinition('layout_section'),
//      'region'
//    ];

//    $event->get
//    $pm = \Drupal::getContainer()->get('plugin.manager.condition');


  }

}
