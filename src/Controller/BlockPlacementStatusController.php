<?php

declare(strict_types = 1);

namespace Drupal\layout_builder_place\Controller;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\layout_builder_place\Entity\BlockPlacementInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Block placement status status changing controller.
 */
class BlockPlacementStatusController extends ControllerBase {

  /**
   * Provides a controller for enable/disable block placements.
   *
   * @param \Drupal\layout_builder_place\Entity\BlockPlacementInterface $block_placement
   *   The block placement config entity.
   * @param string $operation
   *   Allowed values: 'enable', 'disable'.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirects to the list of block placement config entities.
   */
  public function toggleStatus(BlockPlacementInterface $block_placement, string $operation): RedirectResponse {
    $status = [
      'enable' => [
        'value' => TRUE,
        'status' => $this->t('enabled'),
      ],
      'disable' => [
        'value' => FALSE,
        'status' => $this->t('disabled'),
      ],
    ];
    if (!isset($status[$operation])) {
      throw new \InvalidArgumentException("Operation '{$operation}' is invalid. Allowed values: 'enable', 'disable'.");
    }

    $arguments = [
      '%rule' => $block_placement->label(),
      '@label' => $block_placement->getEntityType()->getSingularLabel(),
      '@status' => $status[$operation]['status'],
    ];

    if ($block_placement->get('status') === $status[$operation]['value']) {
      $this->messenger()->addWarning($this->t("The %rule @label is already @status.", $arguments));
    }
    else {
      $this->messenger()->addStatus($this->t("The %rule @label has been @status.", $arguments));
      $block_placement->setStatus($status[$operation]['value'])->save();
    }

    return $this->redirect('entity.block_placement.collection');
  }

  /**
   * Provides a title callback for the enable/disable block placements.
   *
   * @param string $operation
   *   Allowed values: 'enable', 'disable'.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The title.
   */
  public function getStatusOperationTitle(string $operation): MarkupInterface {
    return [
      'enable' => $this->t('Enable'),
      'disable' => $this->t('Disable'),
    ][$operation];
  }

}
