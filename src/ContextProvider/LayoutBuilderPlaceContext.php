<?php

namespace Drupal\layout_builder_place\ContextProvider;

use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Plugin\Context\ContextProviderInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Sets the section's region as a context.
 */
class LayoutBuilderPlaceContext implements ContextProviderInterface {

  use StringTranslationTrait;

  /**
   * The section delta.
   *
   * @var int
   */
  protected $sectionDelta;

  /**
   * The section's region.
   *
   * @var string
   */
  protected $region;

  /**
   * {@inheritdoc}
   */
  public function getRuntimeContexts(array $unqualified_context_ids): array {
    return [
      'section_delta' => new Context(new ContextDefinition('integer'), $this->sectionDelta),
      'region' => new Context(new ContextDefinition('string'), $this->region),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableContexts(): array {
    return [
      'section_delta' => new Context(new ContextDefinition('integer'), $this->t('Section delta')),
      'region' => new Context(new ContextDefinition('string', $this->t('Region'))),
    ];
  }

  /**
   * Sets the section delta.
   *
   * @param int $section_delta
   *   The section delta.
   *
   * @return $this
   */
  public function setSectionDelta(int $section_delta): self {
    $this->sectionDelta = $section_delta;
    return $this;
  }

  /**
   * Sets the section's region.
   *
   * @param string $region
   *   The region identifier.
   *
   * @return $this
   */
  public function setRegion(string $region): self {
    $this->region = $region;
    return $this;
  }

}
