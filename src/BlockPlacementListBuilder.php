<?php

declare(strict_types = 1);

namespace Drupal\layout_builder_place;

use Drupal\Core\Condition\ConditionInterface;
use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides an entity list builder for block placement config entities.
 */
class BlockPlacementListBuilder extends DraggableListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['help'] = [
      '#markup' => $this->t('As the blocks are placed in different layout regions, the block placements order can be better tuned in the layout editor.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    return [
      'label' => $this->t('Placement'),
      'status' => $this->t('Status'),
      'conditions' => $this->t('Conditions'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $block_placement): array {
    /** @var \Drupal\theme_rule\Entity\ThemeRuleInterface $block_placement */
    return [
      'label' => $block_placement->label(),
      'status' => [
        'data' => [
          '#markup' => $block_placement->status() ? $this->t('Enabled') : $this->t('Disabled'),
        ],
      ],
      'conditions' => [
        '#theme' => 'item_list',
        '#items' => array_map(function (ConditionInterface $condition) {
          return $condition->summary();
        }, $block_placement->getConditions()->getIterator()->getArrayCopy()),
      ],
    ] + parent::buildRow($block_placement);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'block_placement_collection';
  }

}
