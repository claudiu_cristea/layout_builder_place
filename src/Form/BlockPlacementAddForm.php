<?php

namespace Drupal\layout_builder_place\Form;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to create a block placement.
 */
class BlockPlacementAddForm extends FormBase {

  /**
   * The block plugin manager service.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockPluginManager;

  /**
   * The UUID generator service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidGenerator;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new form instance.
   *
   * @param \Drupal\Core\Block\BlockManagerInterface $block_plugin_manager
   *   The block plugin manager service.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_generator
   *   The UUID generator service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(BlockManagerInterface $block_plugin_manager, UuidInterface $uuid_generator, EntityTypeManagerInterface $entity_type_manager) {
    $this->blockPluginManager = $block_plugin_manager;
    $this->uuidGenerator = $uuid_generator;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('plugin.manager.block'),
      $container->get('uuid'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['plugin'] = [
      '#type' => 'select',
      '#title' => $this->t('Block plugin'),
      '#options' => $this->getBlockPlugins(),
      '#empty_value' => NULL,
      '#required' => TRUE,
    ];
    $form['actions'] = [
      '#type' => 'actions',
      [
        '#type' => 'submit',
        '#value' => $this->t('Add'),
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $uuid = $this->uuidGenerator->generate();
    $this->entityTypeManager->getStorage('block_placement')->create([
      'id' => $uuid,
      'uuid' => $uuid,
      'plugin' => $form_state->getValue('plugin'),
    ])->save();
    $this->messenger()->addStatus($this->t('The block placement has been created, now you can edit the configuration.'));
    $form_state->setRedirect('entity.block_placement.edit_form', [
      'block_placement' => $uuid,
    ]);
  }

  /**
   * Builds a list of block plugins.
   *
   * @return array
   *   An array suitable to be used in the form API #options property.
   */
  protected function getBlockPlugins(): array {
    $definitions = $this->blockPluginManager->getFilteredDefinitions('layout_builder_place');
    $grouped_definitions = $this->blockPluginManager->getGroupedDefinitions($definitions);
    array_walk($grouped_definitions, function (array &$definitions): void {
      $definitions = array_map(function (array $definition): string {
        return (string) $definition['admin_label'];
      }, $definitions);
    });
    return $grouped_definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'block_placement_add';
  }

}
