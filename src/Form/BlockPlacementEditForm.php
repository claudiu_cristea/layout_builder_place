<?php

declare(strict_types = 1);

namespace Drupal\layout_builder_place\Form;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Condition\ConditionManager;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\Core\Plugin\PluginFormFactoryInterface;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Drupal\layout_builder_place\Entity\BlockPlacement;
use Drupal\layout_builder_place\Entity\BlockPlacementInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form class for block placements.
 */
class BlockPlacementEditForm extends EntityForm {

  /**
   * The block plugin manager service.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockPluginManager;

  /**
   * The module extension list service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * The condition plugin manager service.
   *
   * @var \Drupal\Core\Condition\ConditionManager
   */
  protected $conditionManager;

  /**
   * The context repository service.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected $contextRepository;

  /**
   * The plugin form manager.
   *
   * @var \Drupal\Core\Plugin\PluginFormFactoryInterface
   */
  protected $pluginFormFactory;

  /**
   * Constructs a new form instance.
   *
   * @param \Drupal\Core\Block\BlockManagerInterface $block_plugin_manager
   *   The block plugin manager service.
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extension_list
   *   The module extension list service.
   * @param \Drupal\Core\Condition\ConditionManager $condition_manager
   *   The condition plugin manager service.
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $context_repository
   *   The context repository service.
   * @param \Drupal\Core\Plugin\PluginFormFactoryInterface $plugin_form_manager
   *   The plugin form manager.
   */
  public function __construct(BlockManagerInterface $block_plugin_manager, ModuleExtensionList $module_extension_list, ConditionManager $condition_manager, ContextRepositoryInterface $context_repository, PluginFormFactoryInterface $plugin_form_manager) {
    $this->blockPluginManager = $block_plugin_manager;
    $this->moduleExtensionList = $module_extension_list;
    $this->conditionManager = $condition_manager;
    $this->contextRepository = $context_repository;
    $this->pluginFormFactory = $plugin_form_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('plugin.manager.block'),
      $container->get('extension.list.module'),
      $container->get('plugin.manager.condition'),
      $container->get('context.repository'),
      $container->get('plugin_form.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity(): BlockPlacementInterface {
    // Extending the parent method only to allow proper return typing.
    /** @var \Drupal\layout_builder_place\Entity\BlockPlacementInterface $block_placement */
    $block_placement = parent::getEntity();
    return $block_placement;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $block_placement = $this->getEntity();

    // Store the gathered contexts in the form state for other objects to use
    // during form building.
    $form_state->setTemporaryValue('gathered_contexts', $this->contextRepository->getAvailableContexts());

    $form['#tree'] = TRUE;

    $form['settings'] = [];
    $subform_state = SubformState::createForSubform($form['settings'], $form, $form_state);
    $form['settings'] = $this->getPluginForm($block_placement->getPlugin())->buildConfigurationForm($form['settings'], $subform_state);

    $form['conditions'] = $this->buildConditions([], $form_state);
    $form['weight'] = [
      '#type' => 'weight',
      '#title' => $this->t('Weight'),
      '#default_value' => $block_placement->getWeight(),
      '#delta' => 10,
    ];
    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $block_placement->status(),
    ];

    return $form;
  }

  /**
   * Helper function for building the conditions UI form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form array with the conditions UI added in.
   */
  protected function buildConditions(array $form, FormStateInterface $form_state): array {
    $form['condition_tabs'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Conditions'),
      '#parents' => ['condition_tabs'],
      '#attached' => [
        'library' => [
          'layout_builder_place/conditions',
        ],
      ],
    ];

    $config = $this->getEntity()->getConditionsConfig();
    $definitions = $this->conditionManager->getFilteredDefinitions('block_placement', $form_state->getTemporaryValue('gathered_contexts'), [
      'block_placement' => $this->getEntity(),
    ]);
    foreach ($definitions as $condition_id => $definition) {
      /** @var \Drupal\Core\Condition\ConditionInterface $condition */
      $condition = $this->conditionManager->createInstance($condition_id, $config[$condition_id] ?? []);
      $form_state->set(['conditions', $condition_id], $condition);
      $condition_form = $condition->buildConfigurationForm([], $form_state);
      $condition_form['#type'] = 'details';
      $condition_form['#title'] = $condition->getPluginDefinition()['label'];
      $condition_form['#group'] = 'condition_tabs';
      $form[$condition_id] = $condition_form;
    }

    // These tweaks were copied from BlockForm::buildVisibilityInterface(). In
    // order to alter form element provided by non-core condition plugins, you
    // should alter the form itself.
    if (isset($form['node_type'])) {
      $form['node_type']['#title'] = $this->t('Content types');
      $form['node_type']['bundles']['#title'] = $this->t('Content types');
      $form['node_type']['negate']['#type'] = 'value';
      $form['node_type']['negate']['#title_display'] = 'invisible';
      $form['node_type']['negate']['#value'] = $form['node_type']['negate']['#default_value'];
    }
    if (isset($form['user_role'])) {
      $form['user_role']['#title'] = $this->t('Roles');
      unset($form['user_role']['roles']['#description']);
      $form['user_role']['negate']['#type'] = 'value';
      $form['user_role']['negate']['#value'] = $form['user_role']['negate']['#default_value'];
    }
    if (isset($form['request_path'])) {
      $form['request_path']['#title'] = $this->t('Pages');
      $form['request_path']['negate']['#type'] = 'radios';
      $form['request_path']['negate']['#default_value'] = (int) $form['request_path']['negate']['#default_value'];
      $form['request_path']['negate']['#title_display'] = 'invisible';
      $form['request_path']['negate']['#options'] = [
        $this->t('Show for the listed pages'),
        $this->t('Hide for the listed pages'),
      ];
    }
    if (isset($form['language'])) {
      $form['language']['negate']['#type'] = 'value';
      $form['language']['negate']['#value'] = $form['language']['negate']['#default_value'];
    }
    if (isset($form['current_theme'])) {
      $form['current_theme']['theme']['#empty_option'] = $this->t('- None -');
      $form['current_theme']['theme']['#empty_value'] = NULL;
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);
    // Proper cast status & weight.
    $form_state->setValue('status', (bool) $form_state->getValue('status'));
    $form_state->setValue('weight', (int) $form_state->getValue('weight'));
    $this->getPluginForm($this->entity->getPlugin())->validateConfigurationForm($form['settings'], SubformState::createForSubform($form['settings'], $form, $form_state));
    $this->validateConditions($form, $form_state);
  }

  /**
   * Helper function to independently validate conditions.
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  protected function validateConditions(array $form, FormStateInterface $form_state): void {
    // Validate conditions settings.
    foreach ($form_state->getValue('conditions') as $condition_id => $values) {
      // All condition plugins use 'negate' as a boolean in their schema.
      // However, certain form elements may return it as 0/1. Cast here to
      // ensure the data is in the expected type.
      if (array_key_exists('negate', $values)) {
        $form_state->setValue(['conditions', $condition_id, 'negate'], (bool) $values['negate']);
      }

      // Allow the condition to validate the form.
      $condition = $form_state->get(['conditions', $condition_id]);
      $condition->validateConfigurationForm($form['conditions'][$condition_id], SubformState::createForSubform($form['conditions'][$condition_id], $form, $form_state));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $sub_form_state = SubformState::createForSubform($form['settings'], $form, $form_state);

    // Call the plugin submit handler.
    $block = $this->getEntity()->getPlugin();
    $this->getPluginForm($block)->submitConfigurationForm($form, $sub_form_state);
    // If this block is context-aware, set the context mapping.
    if ($block instanceof ContextAwarePluginInterface && $block->getContextDefinitions()) {
      $context_mapping = $sub_form_state->getValue('context_mapping', []);
      $block->setContextMapping($context_mapping);
    }

    $this->submitConditions($form, $form_state);

    $this->getEntity()->save();
    $this->messenger()->addStatus($this->t('The block placement configuration has been saved.'));
    $form_state->setRedirect('entity.block_placement.collection');
  }

  /**
   * Helper function to independently submit the conditions.
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  protected function submitConditions(array $form, FormStateInterface $form_state): void {
    foreach ($form_state->getValue('conditions') as $condition_id => $values) {
      // Allow the condition to submit the form.
      $condition = $form_state->get(['conditions', $condition_id]);
      $condition->submitConfigurationForm($form['conditions'][$condition_id], SubformState::createForSubform($form['conditions'][$condition_id], $form, $form_state));

      $condition_configuration = $condition->getConfiguration();
      // Update the visibility conditions on the block placement.
      $this->getEntity()->getConditions()->addInstanceId($condition_id, $condition_configuration);
    }
  }

  /**
   * Builds a list of block plugins.
   *
   * @return array
   *   An array suitable to be used in the form API #options property.
   */
  protected function getBlockPlugins(): array {
    $definitions = $this->blockPluginManager->getFilteredDefinitions('layout_builder_place');
    $grouped_definitions = $this->blockPluginManager->getGroupedDefinitions($definitions);
    array_walk($grouped_definitions, function (array &$definitions): void {
      $definitions = array_map(function (array $definition): string {
        return (string) $definition['admin_label'];
      }, $definitions);
    });
    return $grouped_definitions;
  }

  /**
   * Retrieves the plugin form for a given block and operation.
   *
   * @param \Drupal\Core\Block\BlockPluginInterface $block
   *   The block plugin.
   *
   * @return \Drupal\Core\Plugin\PluginFormInterface
   *   The plugin form for the block.
   */
  protected function getPluginForm(BlockPluginInterface $block) {
    if ($block instanceof PluginWithFormsInterface) {
      return $this->pluginFormFactory->createInstance($block, 'configure');
    }
    return $block;
  }

}
