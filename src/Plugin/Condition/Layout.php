<?php

namespace Drupal\layout_builder_place\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\layout_builder\Entity\LayoutEntityDisplayInterface;
use Drupal\layout_builder\Section;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a layout condition.
 *
 * @todo Make this pluggable, for now only the 'defaults' section storage is
 *   covered. Hint: swap the
 *
 * @Condition(
 *   id = "layout_section_region",
 *   label = @Translation("Layout section region"),
 *   context_definitions = {
 *     "section_delta" = @ContextDefinition("integer", label = @Translation("Section delta")),
 *     "region" = @ContextDefinition("string", label = @Translation("Region")),
 *   },
 * )
 */
class Layout extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfo;

  /**
   * The entity view mode storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $entityViewModeStorage;

  /**
   * Constructs a new condition plugin instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info
   *   The entity type bundle info service.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $bundle_info) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->bundleInfo = $bundle_info;
    //$this->sm = \
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'section_storage' => [
        'defaults' => [],
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $entity_view_display_storage = $this->entityTypeManager->getStorage('entity_view_display');
    $options = [];
    /** @var \Drupal\layout_builder\Entity\LayoutBuilderEntityViewDisplay $entity_view_display */
    foreach ($entity_view_display_storage->loadMultiple() as $id => $entity_view_display) {
      if ($entity_view_display->isLayoutBuilderEnabled()) {
        if ($sections = $entity_view_display->getSections()) {
          $group = $this->buildEntityViewDisplayLabel($entity_view_display);
          $options[$group] = $this->buildSectionOptions($id, $sections);
        }
      }
    }

    $form['section_storage'] = [
      '#tree' => TRUE,
    ];
    if ($options) {
      $form['section_storage']['defaults'] = [
        '#type' => 'select',
        '#title' => $this->t('Entity view display regions'),
        '#description' => $this->t('Only layout builder enabled displays are shown'),
        '#options' => $options,
        '#default_value' => $this->getConfiguration()['section_storage']['defaults'],
        '#multiple' => TRUE,
        '#size' => 10,
      ];
    }
    else {
      $form['section_storage']['defaults_empty'] = [
        '#markup' => $this->t('No entity view display is configured to use Layout Builder'),
      ];
      $form['section_storage']['defaults'] = [
        '#type' => 'value',
        '#value' => [],
      ];
    }

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->setConfig('section_storage', $form_state->getValue('section_storage'));
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {

  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
//    $this->getContext('section_delt')
    return FALSE;
  }

  /**
   * Builds entity view display label.
   *
   * @param \Drupal\layout_builder\Entity\LayoutEntityDisplayInterface $entity_view_display
   *   The entity view display instance.
   * @param string $entity_views_display_id
   *   The entity view display ID.
   *
   * @return string
   *   The entity view display label.
   */
  protected function buildEntityViewDisplayLabel(LayoutEntityDisplayInterface $entity_view_display): string {
    $entity_type_id = $entity_view_display->getTargetEntityTypeId();
    $entity_view_mode = $this->getEntityViewModeStorage()->load("{$entity_type_id}.{$entity_view_display->getMode()}");
    $mode = $entity_view_mode ? $entity_view_mode->label() : $this->t('Default');
    $label = [
      $this->entityTypeManager->getDefinition($entity_type_id)->getLabel(),
      $this->bundleInfo->getBundleInfo($entity_type_id)[$entity_view_display->getTargetBundle()]['label'],
      $mode,
    ];
    return implode(' » ', $label);
  }

  protected function buildSectionOptions(string $entity_views_display_id, array $sections): array {
    $options = [];
    foreach ($sections as $delta => $section) {
      $layout_settings = $section->getLayoutSettings();
      $section_label = $layout_settings['label'] ?: $this->t('Section @section', ['@section' => $delta + 1]);
      foreach ($section->getLayout()->getPluginDefinition()->getRegions() as $region => $region_info) {
        $options["{$entity_views_display_id}:{$delta}:{$region}"] = "{$section_label} » {$region_info['label']}";
      }
    }
    return $options;
  }

  /**
   * Returns the entity view mode storage.
   *
   * @return \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   *    The entity view mode storage.
   */
  protected function getEntityViewModeStorage(): ConfigEntityStorageInterface {
    if (!isset($this->entityViewModeStorage)) {
      $this->entityViewModeStorage = $this->entityTypeManager->getStorage('entity_view_mode');
    }
    return $this->entityViewModeStorage;
  }

}
