<?php

namespace Drupal\layout_builder_place\Entity;

use Drupal\block\BlockInterface;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Condition\ConditionInterface;
use Drupal\Core\Condition\ConditionPluginCollection;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;

/**
 * Provides an interface for 'block_placement' entity type.
 */
interface BlockPlacementInterface extends ConfigEntityInterface, EntityWithPluginCollectionInterface {

  /**
   * Returns the plugin instance.
   *
   * @return \Drupal\Core\Block\BlockPluginInterface
   *   The plugin instance for this block.
   */
  public function getPlugin(): BlockPluginInterface;

  /**
   * Returns the plugin ID.
   *
   * @return string|null
   *   The plugin ID for this block or NULL if it hasn't been set yet.
   */
  public function getPluginId(): ?string;

  /**
   * Gets conditions for this placement.
   *
   * @return \Drupal\Core\Condition\ConditionPluginCollection
   *   A collection of configured condition plugins.
   */
  public function getConditions(): ConditionPluginCollection;

  /**
   * Returns an array of condition configurations.
   *
   * @return array
   *   An array of condition configuration keyed by the condition ID.
   */
  public function getConditionsConfig(): array;

  /**
   * Sets the visibility condition configuration.
   *
   * @param string $instance_id
   *   The condition instance ID.
   * @param array $configuration
   *   The condition configuration.
   *
   * @return $this
   */
  public function setConditionsConfig(string $instance_id, array $configuration): self;

  /**
   * Gets a visibility condition plugin instance.
   *
   * @param string $instance_id
   *   The condition plugin instance ID.
   *
   * @return \Drupal\Core\Condition\ConditionInterface
   *   A condition plugin.
   */
  public function getCondition(string $instance_id): ConditionInterface;

  /**
   * Returns the weight of this block placement (used for sorting).
   *
   * @return int
   *   The block placement weight.
   */
  public function getWeight(): int;

  /**
   * Sets the block placement weight.
   *
   * @param int $weight
   *   The block placement weight.
   *
   * @return $this
   */
  public function setWeight(int $weight): self;

}
