<?php

namespace Drupal\layout_builder_place\Entity;

use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Condition\ConditionInterface;
use Drupal\Core\Condition\ConditionManager;
use Drupal\Core\Condition\ConditionPluginCollection;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Plugin\DefaultSingleLazyPluginCollection;
use Drupal\layout_builder_place\BlockPlacementPluginCollection;

/**
 * Defines a block placement configuration entity class.
 *
 * @ConfigEntityType(
 *   id = "block_placement",
 *   label = @Translation("Block placement"),
 *   label_collection = @Translation("Block placements"),
 *   label_singular = @Translation("block placement"),
 *   label_plural = @Translation("block placements"),
 *   label_count = @PluralTranslation(
 *     singular = "@count block placement",
 *     plural = "@count block placements",
 *   ),
 *   config_prefix = "placement",
 *   handlers = {
 *     "access" = "Drupal\layout_builder_place\BlockPlacementAccessControlHandler",
 *     "list_builder" = "Drupal\layout_builder_place\BlockPlacementListBuilder",
 *     "form" = {
 *       "default" = "Drupal\layout_builder_place\Form\BlockPlacementEditForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer block placements",
 *   links = {
 *     "collection" = "/admin/structure/block-placement",
 *     "edit-form" = "/admin/structure/block-placement/manage/{block_placement}",
 *     "delete-form" = "/admin/structure/block-placement/manage/{block_placement}/delete",
 *     "enable" = "/admin/structure/block-placement/manage/{block_placement}/enable",
 *     "disable" = "/admin/structure/block-placement/manage/{block_placement}/disable",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "status" = "status",
 *     "weight" = "weight",
 *   },
 *   config_export = {
 *     "id",
 *     "status",
 *     "weight",
 *     "plugin",
 *     "settings",
 *     "conditions",
 *   },
 * )
 */
class BlockPlacement extends ConfigEntityBase implements BlockPlacementInterface {

  /**
   * The block placement ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The plugin instance ID.
   *
   * @var string
   */
  protected $plugin;

  /**
   * The plugin instance settings.
   *
   * @var array
   */
  protected $settings = [];

  /**
   * The block placement weight.
   *
   * @var int
   */
  protected $weight = 0;

  /**
   * The plugin collection that holds the block plugin for this entity.
   *
   * @var \Drupal\block\BlockPluginCollection
   */
  protected $pluginCollection;

  /**
   * The condition plugin configurations.
   *
   * @var array
   */
  protected $conditions = [];

  /**
   * The available contexts for this placement and its visibility conditions.
   *
   * @var array
   */
  protected $contexts = [];

  /**
   * The condition collection.
   *
   * @var \Drupal\Core\Condition\ConditionPluginCollection
   */
  protected $conditionCollection;

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\Core\Executable\ExecutableManagerInterface
   */
  protected $conditionPluginManager;

  /**
   * {@inheritdoc}
   */
  public function label() {
    $settings = $this->get('settings');
    if ($settings['label']) {
      return $settings['label'];
    }
    else {
      $definition = $this->getPlugin()->getPluginDefinition();
      return $definition['admin_label'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections(): array {
    return [
      'settings' => $this->getPluginCollection(),
      'conditions' => $this->getConditions(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConditions(): ConditionPluginCollection {
    if (!isset($this->conditionCollection)) {
      $this->conditionCollection = new ConditionPluginCollection($this->getConditionPluginManager(), $this->get('conditions'));
    }
    return $this->conditionCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function getConditionsConfig(): array {
    return $this->getConditions()->getConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function setConditionsConfig(string $instance_id, array $configuration): BlockPlacementInterface {
    $conditions = $this->getConditions();
    if (!$conditions->has($instance_id)) {
      $configuration['id'] = $instance_id;
      $conditions->addInstanceId($instance_id, $configuration);
    }
    else {
      $conditions->setInstanceConfiguration($instance_id, $configuration);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCondition(string $instance_id): ConditionInterface {
    return $this->getConditions()->get($instance_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugin(): BlockPluginInterface {
    return $this->getPluginCollection()->get($this->plugin);
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId(): ?string {
    return $this->plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight(): int {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight(int $weight): BlockPlacementInterface {
    $this->weight = $weight;
    return $this;
  }

  /**
   * Encapsulates the creation of the block's LazyPluginCollection.
   *
   * @return \Drupal\Core\Plugin\DefaultSingleLazyPluginCollection
   *   The block's plugin collection.
   */
  protected function getPluginCollection(): DefaultSingleLazyPluginCollection {
    if (!$this->pluginCollection) {
      $this->pluginCollection = new BlockPlacementPluginCollection(
        \Drupal::service('plugin.manager.block'),
        $this->plugin,
        $this->get('settings'),
        $this->id()
      );
    }
    return $this->pluginCollection;
  }

  /**
   * Returns the condition plugin manager.
   *
   * @return \Drupal\Core\Condition\ConditionManager
   *   The condition plugin manager.
   */
  protected function getConditionPluginManager(): ConditionManager {
    if (!isset($this->conditionPluginManager)) {
      $this->conditionPluginManager = \Drupal::service('plugin.manager.condition');
    }
    return $this->conditionPluginManager;
  }

}
