<?php

namespace Drupal\Tests\layout_builder_place\Kernel;

use Drupal\block\Entity\Block;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\KernelTests\KernelTestBase;
use Drupal\layout_builder\Entity\LayoutBuilderEntityViewDisplay;
use Drupal\layout_builder\Section;
use Drupal\layout_builder_place\Entity\BlockPlacement;

/**
 * Tests @todo .....
 *
 * @group layout_builder_place
 */
class LayoutBuilderPlaceTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    // @todo sort
    'block',
    'layout_builder_place',
    'layout_builder',
    'system',
    'entity_test',
    'layout_discovery',

    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installEntitySchema('entity_test');
  }

  /**
   * Tests @todo .....
   */
  public function testLayoutBuilderBlockPlacement(): void {
//    $section_storage_manager = $this->container->get('plugin.manager.layout_builder.section_storage');
    $display = LayoutBuilderEntityViewDisplay::create([
      'status' => TRUE,
      'targetEntityType' => 'entity_test',
      'bundle' => 'entity_test',
      'mode' => 'default',
    ]);
    $display
      ->enableLayoutBuilder()
      ->appendSection(new Section('layout_onecol', ['label' => 'sec111']))
      ->save();


    $this->assertTrue($display->isLayoutBuilderEnabled());


    //    /** @var \Drupal\layout_builder\SectionStorageInterface $section_storage */
    //    $section_storage = $section_storage_manager->load('defaults', [
    //      'display' => EntityContext::fromEntity($display),
    //    ]);
    //    $section_storage->

    $placement = BlockPlacement::create([
      'id' => 'foo',
      'plugin' => 'system_powered_by_block',
    ]);
    $placement->setConditionsConfig('layout_section_region', [
      'id' => 'layout_section_region',
      'negate' => FALSE,
      'section_storage' => [
        'defaults' => [
          'entity_test.entity_test.default:0:content',
        ],
      ],
      'context_mapping' => [
        'section_delta' => '@layout_builder_place.context:section_delta',
        'region' => '@layout_builder_place.context:region',
      ],
    ]);
    $placement->save();

    $entity = EntityTest::create([
      'name' => $this->randomString(),
      'type' => 'entity_test',
    ]);
    $entity->save();

    /** @var \Drupal\Core\Entity\EntityViewBuilderInterface $view_builder */
    $view_builder = $this->container->get('entity_type.manager')->getViewBuilder('entity_test');
    $render_array = $view_builder->view($entity);
    $rendered = $this->container->get('renderer')->renderPlain($render_array);
    $this->assertStringContainsString(Html::escape($entity->label()), $rendered);
  }

}

